#include "BSNode.h"
#include <iostream>

using std::cout;
using std::endl;


int main()
{
	int intArr[15] = { 5, 4, 6, 1, 8, 145, 61, 143, 13, 72, 73, 256, 714, 75, 44 };
	std::string stringArr[15] = { "abc", "fdfda", "baxzc", "jrgfd", "darf", "zcsad", "kdfhfd", "qwedafa", "Fdsjf", "jfgjfd", "uetfgv", "khgkhgf", "nvnfv", "yrew", "iyrghj" };
	BSNode<std::string>* stringTree = new BSNode<std::string>("abc");
	BSNode<int>* intTree = new BSNode<int>(5);
	for (int i = 0; i < 15; i++) 
	{
		std::cout << intArr[i] << " ";
		if (i != 0)
		{
			intTree->insert(intArr[i]);
		}
	}
	cout << "\n";
	for (int i = 0; i < 15; i++) 
	{
		std::cout << stringArr[i] << " ";
		if (i != 0)
		{
			stringTree->insert(stringArr[i]);
		}
	}
	cout << "\n";
	stringTree->printNodes();
	intTree->printNodes();
	return 0;
}
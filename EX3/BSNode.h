#pragma once

#include <iostream>
#include <string>

template <class T>
class BSNode
{
public:
	BSNode(T data);
	BSNode(const BSNode& other);

	~BSNode();
	
	void insert(T value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	T getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;

	bool search(T val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;
	int getCount() const;
	void printNodes() const; //for question 1 part C

private:
	T _data;
	BSNode* _left;
	BSNode* _right;

	int _count; //for question 1 part B
};

template<class T>
inline BSNode<T>::BSNode(T data)
{
	this->_data = data;
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 1;
}

template<class T>
inline BSNode<T>::BSNode(const BSNode& other)
{
	if (other.getRight())
		this->_right = new BSNode(*other.getRight());
	if (other.getLeft())
		this->_left = new BSNode(*other.getLeft());
}

template<class T>
inline BSNode<T>::~BSNode()
{
	free(this);
}

template<class T>
inline void BSNode<T>::insert(T value)
{
	if (value > this->_data)
	{
		if (this->_right == nullptr)
		{
			this->_right = new BSNode(value);
		}
		else
		{
			this->_right->insert(value);
		}
	}
	else if (value < this->_data)
	{
		if (this->_left == nullptr)
		{
			this->_left = new BSNode(value);
		}
		else
		{
			this->_left->insert(value);
		}
	}
	else
	{
		this->_count++;
	}
}

template<class T>
inline BSNode<T>& BSNode<T>::operator=(const BSNode& other)
{
	this->_data = other.getData();
	this->_count = other.getCount();

	this->_left = new BSNode(*other.getLeft());
	this->_right = new BSNode(*other.getRight());

	return *this;
}

template<class T>
inline bool BSNode<T>::isLeaf() const
{
	return (getRight() == nullptr && getLeft() == nullptr);
}

template<class T>
inline T BSNode<T>::getData() const
{
	return this->_data;
}

template<class T>
inline BSNode<T>* BSNode<T>::getLeft() const
{
	return this->_left;
}

template<class T>
inline BSNode<T>* BSNode<T>::getRight() const
{
	return this->_right;
}

template<class T>
inline bool BSNode<T>::search(T val) const
{
	if (this == nullptr)
	{
		return false;
	}
	else if (this->_data < val)
	{
		return this->_right->search(val);
	}
	else if (this->_data > val)
	{
		return this->_left->search(val);
	}
	else
	{
		return true;
	}
}

template<class T>
inline int BSNode<T>::getHeight() const
{
	int rightSum = 0;
	int leftSum = 0;
	//declaring vars
	if (this->getRight() != nullptr)
	{
		rightSum = this->getRight()->getHeight() + 1;
	}
	if (this->getLeft() != nullptr)
	{
		leftSum = this->getLeft()->getHeight() + 1;
	}
	if (rightSum > leftSum)
	{
		return rightSum;
	}
	return leftSum;
}

template<class T>
inline int BSNode<T>::getDepth(const BSNode& root) const
{
	if (!(root.search(this->_data)))
	{
		return -1;
	}
	return root.getHeight() - this->getHeight();
}

template<class T>
inline int BSNode<T>::getCount() const
{
	return this->_count;
}

template<class T>
inline void BSNode<T>::printNodes() const
{
	if (this == NULL)
		return;

	/* first recur on left child */
	this->_left->printNodes();

	/* then print the data of node */
	std::cout << this->_data << ": " << this->_count << std::endl;

	/* now recur on right child */
	this->_right->printNodes();
}

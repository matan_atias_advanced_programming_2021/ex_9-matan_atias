#pragma once

#include <iostream>


class ilay
{
	public:
		bool operator<(const ilay& other);
		bool operator==(const ilay& other);
		ilay& operator=(const ilay& other);
		friend std::ostream& operator<<(std::ostream& os, const ilay& other);
		int item;
		ilay(int item);
};


/*
	Constructor
	Input:
		item
	Output:
		None
*/
ilay::ilay(int item)
{
	this->item = item;
}

bool ilay::operator<(const ilay& other)
{
	if (this->item < other.item)
	{
		return true;
	}
	return false;
}


bool ilay::operator==(const ilay& other)
{
	if (this->item == other.item)
	{
		return true;
	}
	return false;
}


ilay& ilay:: operator=(const ilay& other)
{
	this->item = other.item;
	return *this;
}


std::ostream& operator<<(std::ostream& os, const ilay& other)
{
	os << other.item;
	return os;
}



/*
	Function takes 2 params and checks which one is bigger
	Input:
		2 params
	Output:
		-1 if param1 is bigger than param2, 0 if they are equal, 1 if param1 is smaller than param2
*/
template <class T>
int compare(T param1, T param2)
{
	if (param1 > param2)
	{
		return -1;
	}
	if (param2 == param1)
	{
		return 0;
	}
	return 1;
}


/*
	function swaps 2 values
	Input:
		2 values
	Output:
		None
*/
template <class T>
void swap(T* xp, T* yp)
{
	T temp = *xp;
	*xp = *yp;
	*yp = temp;
}


/*
	Function bubbleSorts an array
	Input:
		array, array size
	Output:
		None
*/
template <class T>
void bubbleSort(T arr[], T n)
{
	int i = 0;
	int j = 0;
	//declaring vars
	for (i = 0; i < n - 1; i++)
	{
		for (j = 0; j < n - i - 1; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				swap(&arr[j], &arr[j + 1]);
			}
		}
	}
}



/*
	Function prints an array
	Input:
		array, array size
	Output:
		None
*/
template <class T>
void printArray(T arr[], T n)
{
	int i = 0;
	for (i = 0; i < n; i++)
	{
		std::cout << arr[i] << std::endl;
	}
}
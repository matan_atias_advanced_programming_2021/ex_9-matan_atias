#include "functions.h"
#include <iostream>



int main() 
{
	ilay test1(1);
	ilay test2(2);
	std::cout << test1 << std::endl;
	std::cout << test2 << std::endl;
	std::cout << (test2 < test1) << std::endl;
	std::cout << (test2 == test1) << std::endl;
	test1 = test2;
	std::cout << (test2 == test1) << std::endl;
	//checking class 
	std::cout << "correct print is 1 -1 0 1" << std::endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;
	std::cout << compare<char>('a', 'g') << std::endl;
	//check compare

	std::cout << "correct print is sorted array" << std::endl;
	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	char charArr[arr_size] = { 'a', 'c', 'b', 'z', 'x' };
	bubbleSort<double>(doubleArr, arr_size);
	bubbleSort<char>(charArr, arr_size);
	for ( int i = 0; i < arr_size; i++ ) {
		std::cout << doubleArr[i] << " ";
	}
	for (int i = 0; i < arr_size; i++) {
		std::cout << charArr[i] << " ";
	}
	std::cout << std::endl;
	//check bubbleSort

	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	printArray<char>(charArr, arr_size);
	std::cout << std::endl;
	//check printArray
	system("pause");
	return 1;
}
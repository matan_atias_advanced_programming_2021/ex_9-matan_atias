#include "BSNode.h"
#include <iostream>

using std::cout;
using std::endl;


/*
	Constructor
	Input:
		data
	Output:
		None
*/
BSNode::BSNode(std::string data)
{
	this->_data = data;
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 1;
}


/*
	Copy intersection
	Input:
		other tree
	Output:
		None
*/
BSNode::BSNode(const BSNode& other)
{
	if (other.getRight())
		this->_right = new BSNode(*other.getRight());
	if (other.getLeft())
		this->_left = new BSNode(*other.getLeft());
}


/*
	Destructor
	Input:
		None
	Output:
		None
*/
BSNode::~BSNode()
{
	free(this);
}


/*
	function inserts a value into the tree
	Input:
		value
	Output:
		None
*/
void BSNode::insert(std::string value)
{
	if (value > this->_data)
	{
		if (this->_right == nullptr)
		{
			this->_right = new BSNode(value);
		}
		else
		{
			this->_right->insert(value);
		}
	}
	else if(value < this->_data)
	{
		if (this->_left == nullptr)
		{
			this->_left = new BSNode(value);
		}
		else
		{
			this->_left->insert(value);
		}
	}
	else
	{
		this->_count++;
	}
}


/*
	Function deep copies a binary tree
	Input:
		other tree
	Output:
		new tree
*/
BSNode& BSNode::operator=(const BSNode& other)
{
	this->_data = other.getData();
	this->_count = other.getCount();

	this->_left = new BSNode(*other.getLeft());
	this->_right = new BSNode(*other.getRight());

	return *this;

}


/*
	returns if an intersection is a leaf or not
	Input:
		None
	Output:
		true if intersection is a leaf, false if not
*/
bool BSNode::isLeaf() const
{
	return (getRight() == nullptr && getLeft() == nullptr);
}


/*
	returns intersection's value
	Input:
		None
	Output:
		intersection's value
*/
std::string BSNode::getData() const
{
	return this->_data;
}


/*
	retruns left son
	Input:
		None
	Output:
		left son
*/
BSNode* BSNode::getLeft() const
{
	return this->_left;
}


/*
	retruns right son
	Input:
		None
	Output:
		right son
*/
BSNode* BSNode::getRight() const
{
	return this->_right;
}


/*
	Setter for count
	Input:
		count
	Output:
		None
*/
int BSNode::getCount() const
{
	return this->_count;
}


/*
	function checks if a value is in the tree
	Input:
		Value
	Output:
		true if value is in the tree, false if it isn't
*/
bool BSNode::search(std::string val) const
{
	if (this == nullptr)
	{
		return false;
	}
	else if (this->_data < val)
	{
		return this->_right->search(val);
	}
	else if (this->_data > val)
	{
		return this->_left->search(val);
	}
	else
	{
		return true;
	}
}


/*
	function returns the height of the tree
	Input:
		None
	Output:
		height of the tree
*/
int BSNode::getHeight() const
{
	int rightSum = 0;
	int leftSum = 0;
	//declaring vars
	if (this->getRight() != nullptr)
	{
		rightSum = this->getRight()->getHeight() + 1;
	}
	if (this->getLeft() != nullptr)
	{
		leftSum = this->getLeft()->getHeight() + 1;
	}
	if (rightSum > leftSum)
	{
		return rightSum;
	}
	return leftSum;
}


/*
	function returns the depth of the tree
	Input:
		root
	Output:
		depth of the tree
*/
int BSNode::getDepth(const BSNode& root) const
{
	if (!(root.search(this->_data)))
	{
		return -1;
	}
	return root.getHeight() - this->getHeight();
}


/*
	printing the tree
	Input:
		None
	Output:
		None
*/
void BSNode::printNodes() const
{
	if (this == NULL)
		return;

	/* first recur on left child */
	this->_left->printNodes();

	/* then print the data of node */
	cout << this->_data << ": " << this->_count << endl;

	/* now recur on right child */
	this->_right->printNodes();
}